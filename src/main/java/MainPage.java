import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.WebElement;

import java.util.List;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;

public class MainPage extends XPathForMainPage {

    ElementsCollection allCategories = $$x(categories);

    public void goToCategory() {
        $x(ehlektronika).click();
    }

    public void allCategories() {
        allCategories.shouldHave(size(17));
    }

    public void checkRelinkOfAllCategories() {
        for (SelenideElement item : allCategories) {

            SelenideElement getUrl = item.$x(categories);
            $x(categories).click();

            String currentUrl = url();

        }
    }

}

